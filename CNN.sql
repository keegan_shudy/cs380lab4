CREATE TABLE Weather(
  day date PRIMARY KEY,
   temp int not NULL,
   description varchar(10),
   city varchar(30),
   state varchar(30)
);


CREATE TABLE News
(
   day date PRIMARY KEY,
   anchor varchar(15) not NULL,
   coanchor varchar(15),
   headline varchar(100) not NULL  , 
   headline_type varchar(20) not NULL,
   city varchar(30),
   state varchar(30)
);


